%global with_broadway 0
%bcond_with docs

%global glib2_version 2.76.0
%global pango_version 1.50.0
%global cairo_version 1.14.0
%global gdk_pixbuf_version 2.30.0
%global wayland_protocols_version 1.31
%global wayland_version 1.21.0
%global epoxy_version 1.4

%global bin_version 4.0.0

%global __provides_exclude_from ^%{_libdir}/gtk-4.0

Summary:        GTK graphical user interface library
Name:           gtk4
Version:        4.12.5
Release:        3%{?dist}
License:        LGPLv2+
URL:            https://www.gtk.org
Source0:        https://download.gnome.org/sources/gtk/4.9/gtk-%{version}.tar.xz
Source1:        settings.ini


%if %{with docs}
BuildRequires:  docbook-style-xsl, gi-docgen
%endif

BuildRequires:  gcc, gcc-c++, meson, gettext, /usr/bin/rst2man
BuildRequires:  cups-devel, desktop-file-utils, python3-gobject, pkgconfig(avahi-gobject)
BuildRequires:  pkgconfig(cairo) >= %{cairo_version}, pkgconfig(cairo-gobject) >= %{cairo_version}, pkgconfig(colord), pkgconfig(egl), pkgconfig(epoxy)
BuildRequires:  pkgconfig(gdk-pixbuf-2.0) >= %{gdk_pixbuf_version}
BuildRequires:  pkgconfig(glib-2.0) >= %{glib2_version}, pkgconfig(gobject-introspection-1.0), pkgconfig(graphene-gobject-1.0), pkgconfig(gstreamer-player-1.0), pkgconfig(json-glib-1.0)
BuildRequires:  pkgconfig(libjpeg), pkgconfig(libpng), pkgconfig(libtiff-4), pkgconfig(pango) >= %{pango_version}
BuildRequires:  pkgconfig(rest-1.0), pkgconfig(sysprof-4), pkgconfig(sysprof-capture-4), pkgconfig(tracker-sparql-3.0)
BuildRequires:  pkgconfig(wayland-client) >= %{wayland_version}, pkgconfig(wayland-cursor) >= %{wayland_version}, pkgconfig(wayland-egl) >= %{wayland_version}, pkgconfig(wayland-protocols) >= %{wayland_protocols_version}
BuildRequires:  pkgconfig(xcomposite), pkgconfig(xcursor), pkgconfig(xdamage), pkgconfig(xfixes), pkgconfig(xi), pkgconfig(xinerama), pkgconfig(xkbcommon), pkgconfig(xrandr), pkgconfig(xrender)
BuildRequires:  libappstream-glib

Requires: adwaita-icon-theme
Requires: hicolor-icon-theme
Requires: gtk-update-icon-cache

Requires: cairo >= %{cairo_version}
Requires: cairo-gobject >= %{cairo_version}
Requires: glib2 >= %{glib2_version}
Requires: libepoxy >= %{epoxy_version}
Requires: libwayland-client >= %{wayland_version}
Requires: libwayland-cursor >= %{wayland_version}
Requires: pango >= %{pango_version}
Requires: gdk-pixbuf2-modules

Recommends: dconf

%description
GTK is a multi-platform toolkit for creating graphical user
interfaces. Offering a complete set of widgets, GTK is suitable for
projects ranging from small one-off tools to complete application
suites.

This package contains version 4 of GTK.

%package devel
Summary: Development files for GTK
Requires: gtk4 = %{version}-%{release}

%description devel
This package contains the libraries and header files that are needed
for writing applications with version 4 of the GTK widget toolkit.

%if %{with docs}
%package devel-docs
Summary: Developer documentation for GTK
BuildArch: noarch
Requires: gtk4 = %{version}-%{release}

%description devel-docs
This package contains developer documentation for version 4 of the GTK
widget toolkit.
%endif

%package devel-tools
Summary: Developer tools for GTK
Requires: gtk4 = %{version}-%{release}

%description devel-tools
This package contains helpful applications for developers using GTK.



%prep
%autosetup -p1 -n gtk-%{version}



%build
export CFLAGS='-fno-strict-aliasing -DG_DISABLE_CAST_CHECKS -DG_DISABLE_ASSERT %optflags'
%meson \
%if 0%{?with_broadway}
        -Dbroadway-backend=true \
%endif
        -Dsysprof=enabled \
        -Dtracker=enabled \
        -Dcolord=enabled \
%if %{with docs}
        -Dgtk_doc=true \
%else
        -Dgtk_doc=false \
%endif
        -Dman-pages=true

%meson_build



%install
%meson_install

%find_lang gtk40

%if !0%{?with_broadway}
rm $RPM_BUILD_ROOT%{_mandir}/man1/gtk4-broadwayd.1*
%endif

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/gtk-4.0
mkdir -p $RPM_BUILD_ROOT%{_libdir}/gtk-4.0/modules

install -p %{SOURCE1} $RPM_BUILD_ROOT%{_datadir}/gtk-4.0/



%check
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/*.xml
desktop-file-validate %{buildroot}%{_datadir}/applications/*.desktop



%files -f gtk40.lang
%license COPYING
%doc AUTHORS NEWS README.md
%{_bindir}/gtk4-launch
%{_bindir}/gtk4-update-icon-cache
%{_libdir}/libgtk-4.so.1*
%dir %{_libdir}/gtk-4.0
%dir %{_libdir}/gtk-4.0/%{bin_version}
%{_libdir}/gtk-4.0/%{bin_version}/media/
%{_libdir}/gtk-4.0/%{bin_version}/printbackends/
%{_libdir}/gtk-4.0/modules
%{_libdir}/girepository-1.0
%{_mandir}/man1/gtk4-launch.1*
%{_mandir}/man1/gtk4-update-icon-cache.1*
%{_datadir}/glib-2.0/schemas/org.gtk.gtk4.Settings.ColorChooser.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gtk.gtk4.Settings.Debug.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gtk.gtk4.Settings.EmojiChooser.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gtk.gtk4.Settings.FileChooser.gschema.xml
%dir %{_datadir}/gtk-4.0
%{_datadir}/gtk-4.0/emoji/
%{_datadir}/gtk-4.0/settings.ini
%if 0%{?with_broadway}
%{_bindir}/gtk4-broadwayd
%{_mandir}/man1/gtk4-broadwayd.1*
%endif

%files devel
%{_libdir}/libgtk-4.so
%{_includedir}/*
%{_libdir}/pkgconfig/*
%{_bindir}/gtk4-builder-tool
%{_bindir}/gtk4-encode-symbolic-svg
%{_bindir}/gtk4-query-settings
%{_datadir}/gettext/
%{_datadir}/gir-1.0
%{_datadir}/gtk-4.0/gtk4builder.rng
%{_datadir}/gtk-4.0/valgrind/
%{_mandir}/man1/gtk4-builder-tool.1*
%{_mandir}/man1/gtk4-encode-symbolic-svg.1*
%{_mandir}/man1/gtk4-query-settings.1*

%if %{with docs}
%files devel-docs
%{_datadir}/doc/gdk4/
%{_datadir}/doc/gdk4-wayland/
%{_datadir}/doc/gdk4-x11/
%{_datadir}/doc/gsk4/
%{_datadir}/doc/gtk4/
%endif

%files devel-tools
%{_bindir}/gtk4-demo
%{_bindir}/gtk4-demo-application
%{_bindir}/gtk4-icon-browser
%{_bindir}/gtk4-node-editor
%{_bindir}/gtk4-print-editor
%{_bindir}/gtk4-rendernode-tool
%{_bindir}/gtk4-widget-factory
%{_datadir}/applications/org.gtk.gtk4.NodeEditor.desktop
%{_datadir}/applications/org.gtk.Demo4.desktop
%{_datadir}/applications/org.gtk.IconBrowser4.desktop
%{_datadir}/applications/org.gtk.PrintEditor4.desktop
%{_datadir}/applications/org.gtk.WidgetFactory4.desktop
%{_datadir}/icons/hicolor/*/apps/org.gtk.gtk4.NodeEditor*.svg
%{_datadir}/icons/hicolor/*/apps/org.gtk.Demo4*.svg
%{_datadir}/icons/hicolor/*/apps/org.gtk.IconBrowser4*.svg
%{_datadir}/icons/hicolor/*/apps/org.gtk.PrintEditor4*.svg
%{_datadir}/icons/hicolor/*/apps/org.gtk.WidgetFactory4*.svg
%{_datadir}/glib-2.0/schemas/org.gtk.Demo4.gschema.xml
%{_metainfodir}/org.gtk.gtk4.NodeEditor.appdata.xml
%{_metainfodir}/org.gtk.Demo4.appdata.xml
%{_metainfodir}/org.gtk.IconBrowser4.appdata.xml
%{_metainfodir}/org.gtk.PrintEditor4.appdata.xml
%{_metainfodir}/org.gtk.WidgetFactory4.appdata.xml
%{_mandir}/man1/gtk4-demo.1*
%{_mandir}/man1/gtk4-demo-application.1*
%{_mandir}/man1/gtk4-icon-browser.1*
%{_mandir}/man1/gtk4-node-editor.1*
%{_mandir}/man1/gtk4-rendernode-tool.1*
%{_mandir}/man1/gtk4-widget-factory.1*



%changelog
* Fri Dec 20 2024 Rebuild Robot <rebot@opencloudos.org> - 4.12.5-3
- [Type] other
- [DESC] Rebuilt for harfbuzz

* Mon Oct 28 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.12.5-2
- [Type] other
- [DESC] bump version for building

* Thu Oct 24 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.12.5-1
- [Type] other
- [DESC] upgrade to version 4.12.5

* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.11.4-6
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.11.4-5
- Rebuilt for loongarch release

* Tue Apr 23 2024 Rebuild Robot <rebot@opencloudos.org> - 4.11.4-4
- Rebuilt for gstreamer1-plugins-bad-free

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.11.4-3
- Rebuilt for OpenCloudOS Stream 23.09

* Wed Aug 02 2023 kianli <kianli@tencent.com> - 4.11.4-2
- Rebuilt for libtiff 4.5.1

* Mon Jul 31 2023 Shuo Wang <abushwang@tencent.com> - 4.11.4-1
- update to 4.11.4

* Mon Jul 17 2023 Xiaojie Chen <jackxjchen@tencent.com> - 4.9.1-4
- Rebuilt for gstreamer1-plugins-base 1.22.4

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.9.1-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.9.1-2
- Rebuilt for OpenCloudOS Stream 23

* Mon Dec 5 2022 Zhao Zhen <jeremiazhao@tencent.com> - 4.9.1-1
- initial
